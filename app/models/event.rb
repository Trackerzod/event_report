class Event < ActiveRecord::Base
  belongs_to :domain
  belongs_to :browser
end