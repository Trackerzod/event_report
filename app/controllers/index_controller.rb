class IndexController < ApplicationController

  def index
    @events = Event.select(:event_type).order(:event_type).distinct
    @browsers = Browser.all
    @domains = Domain.all
  end

  def get_events
    # getting params
    user_agent = params[:browser]
    domain = params[:domain]
    event_type = params[:event_type]
    date_start = params[:date_start]
    date_end = params[:date_end]
    group_date = (params[:group_date]).to_i
    group_hour = (params[:group_hour]).to_i
    group_event_type = (params[:group_event_type]).to_i
    group_browser = (params[:group_browser]).to_i
    group_domain = (params[:group_domain]).to_i

    # getting events depending on condition
    @events = Event.all
    @events = @events.where(event_type: event_type) unless event_type.blank?
    @events = @events.where(browser_id: user_agent) unless user_agent.blank?
    @events = @events.where(domain_id: domain) unless domain.blank?
    @events = @events.where("event_date <= '#{date_end}'") unless date_end.blank?
    @events = @events.where("event_date >= '#{date_start}'") unless date_start.blank?

    # grouping depending on checked checkboxes
    @grouped_events_by_date = group_date == 1 ? @events.group(:event_date).count : nil
    @grouped_events_by_hour = group_hour == 1 ? @events.group(:event_hour).count : nil
    @grouped_events_by_event_type = group_event_type == 1 ? @events.group(:event_type).count : nil
    @grouped_events_by_browser = group_browser == 1 ? @events.group(:browser).count : nil
    @grouped_events_by_domain = group_domain == 1 ? @events.group(:domain).count : nil

    respond_to do |format|
      format.html
      format.js
      format.json { render :json => {
          events: @events,
          grouped_by_date: @grouped_events_by_date,
          grouped_by_hour: @grouped_events_by_hour,
          grouped_by_event_type: @grouped_events_by_event_type,
          grouped_by_browser: @grouped_events_by_browser,
          grouped_by_domain: @grouped_events_by_domain
      }
      }
    end
  end

end
