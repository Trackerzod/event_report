# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).ready ->
  $('#date_start').datetimepicker
    format: 'DD.MM.YYYY',
    locale: 'ru'
  $('#date_end').datetimepicker
    format: 'DD.MM.YYYY',
    useCurrent: false,
    locale: 'ru'

  $('#date_start').on 'dp.change', (e) ->
    $('#date_end').data('DateTimePicker').minDate e.date
    return
  $('#date_end').on 'dp.change', (e) ->
    $('#date_start').data('DateTimePicker').maxDate e.date
    return

  $('#event_report_form').submit (e) ->
    e.preventDefault()
    $.ajax '/event',
      type: 'get'
      dataType: 'json'
      success: (data, textStatus, jqXHR) ->
        clearBlock($('#events tbody'))
        clearBlock($('#table_group_date tbody'))
        clearBlock($('#table_group_hour tbody'))
        clearBlock($('#table_group_event_type tbody'))
        clearBlock($('#table_group_browser tbody'))
        clearBlock($('#table_group_domain tbody'))
        $('.no-content').remove()

  $('#event_report_form').bind 'ajax:complete', ->
    showOrHideBlock($('.report_events'))
    showOrHideBlock($('.group_date'))
    showOrHideBlock($('.group_hour'))
    showOrHideBlock($('.group_event_type'))
    showOrHideBlock($('.group_browser'))
    showOrHideBlock($('.group_domain'))

  showOrHideBlock = (selector) ->
    kids = selector.find("tbody").children()
    if kids.length > 0
      selector.show()
    else
      selector.hide()

  clearBlock = (selector) ->
    selector.children().remove()